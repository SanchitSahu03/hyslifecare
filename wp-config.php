<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hyslifec_hyslifecare');

/** MySQL database username */
define('DB_USER', 'hyslifec_hyslife');
//define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'HYSLifecare@123');
//define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i2!5)ZIGjJPV?1F(qHr]q=5K9%i2+d&7P(uTj~pAq1#g#3(K@5{(5C5$`!JWn6^Q');
define('SECURE_AUTH_KEY',  'Hk:UQan@TsH b615=dW-Qsg)H1?_;SZn`& }qJ@!>iG8648#d!xkBK`@S, X7l^a');
define('LOGGED_IN_KEY',    'EnA5Kge&E$#YBL-m0_sz_ ^1UO86#k29[M;c}Fv5euq/-IqJ4T;zpYU;i<$qLCx{');
define('NONCE_KEY',        ' /`aui$vsTEGh&oIB3E$s|oK5v#D;QfHSTR4W}7d*H20)%!c4Nn1PB;591?yr%$:');
define('AUTH_SALT',        '/I&8qW.a?KgS6RT.umpL;l_w@z%Lv_KZZHBB+|0GY!F[r*qae]d;;a}s0yHN*evS');
define('SECURE_AUTH_SALT', 'yenagVDPsHQKD:9M-Fot-wE=QwP?HA)i9fPnvyS<lT7/EP]ouS6]s?*}t)jM_X&o');
define('LOGGED_IN_SALT',   ')U5JOv2;0U_J$)dEX 7aX2Y:rn+|I5+,29}^vhTGcVhNW)M;(3`1#(|o~^J#guM:');
define('NONCE_SALT',       'L[u  ^_(iVgxTf!a=s@2]eQ&M<B$~wlb*H/i[&-2i2u-kKpXC*F<h6V/L!Ff)^+[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
